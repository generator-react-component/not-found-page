import React, { PropTypes } from "react"

import dependencies from "./dependencies"
import variables from "../variables"

let {Layout} = dependencies
const PageNotFound = React.createClass({
  mixins: [Layout],
  onClick(e) {
    e.preventDefault()
    if (this.props.clickAfter) {
      this.props.clickAfter()
    }
  },
  render() {
    return (
      <div className="page-not-found" {...this.fill} {...this.layoutAlign("center center")} {...this.flex()} {...this.layout("row")}>
        <div className="container" {...this.layoutAlign("center center")} {...this.flex()}>
          <div className="icon-container">
            {typeof this.props.icon === "string" ? (
              <div className="icon">
                <img src={`${variables.imageUrl}${this.props.icon}`} className="tossing" alt="" />
              </div>
            ) : this.props.icon}
            <div className="text">
              <h1>{this.props.title || `404 Page not found`}</h1>
              <h2>{this.props.description || `The page you are looking for is not available.`}</h2>
              <p>{this.props.explanation}</p>
              {this.props.onClick ? <button type="button" className="button" onClick={this.onClick}>{this.props.actionTitle || "OK"}</button> : null}
            </div>
          </div>
        </div>
      </div>
    )
  }
})

export default PageNotFound
