module.exports = {
  Name: "PageNotFound",
  Description: "Page not found",
  Repository: "git@bitbucket.org:jasonvillalon/not-found-page.git",
  AtomicDeps: [
  {
    "Name": "Layout",
    "Description": "Layout",
    "Author": "Jason Villalon",
    "Repository": "git@bitbucket.org:generator-react-component/ac-layout.git",
    "cssVariables": {
      "$baseline-grid": "8px",
      "$layout-breakpoint-sm": "600px",
      "$layout-breakpoint-md": "960px",
      "$layout-breakpoint-lg": "1200px",
      "$layout-gutter-width": "$baseline-grid * 2"
    }
  }
]
};
